import { html, render } from 'lit'
import { $, expect } from '@wdio/globals'

import '../src/my-element.js'

render(
    html`<my-element></my-element>`,
    document.body
)
const status_message_el = await $('my-element')
const button_el = await status_message_el.$('>>>button')
describe('Check if tests are run', () => {
    it('should increase count by clicking on button', async () => {
        await expect(button_el).toHaveText('count is 0')
        button_el.click()
        await expect(button_el).toHaveText('count is 1')
    })

})
